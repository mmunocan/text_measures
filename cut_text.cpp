#include <iostream>
#include <fstream>
#include <cassert>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 4){
		cerr << "ERROR! USE " << argv[0] << " <input_filename> <output_filename> <n>" << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	string output_filename = argv[2];
	int size = atoi(argv[3]);
	
	if(size <= 0){
		cerr << "Value n = " << size << "is invalid" << endl;
		return -1;
	}
	
	ifstream input;
	input.open(input_filename, ios::binary);
	assert(input.is_open() && input.good());
	char * data = new char[size];
	input.read(data, size);
	input.close();
	ofstream output;
	output.open(output_filename, ios::binary);
	assert(output.is_open() && output.good());
	output.write(data, size);
	output.close();
	
	return 0;
}