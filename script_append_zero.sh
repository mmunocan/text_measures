#!/usr/bin/bash

if (($# != 3));
then
	echo "Error! include <origin_path> <output_path> <list_files>"
	exit
fi

origin_path=$1
output_path=$2
list_files=$3

i=0
while IFS= read -r line
do
	set -- $line
		filename=$1
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		# inicalizacion
		nohup ./append_zero "$origin_path/n_$filename" "$origin_path/n0_$filename" &
		
		
		i=$(($i + 1))
done < $list_files