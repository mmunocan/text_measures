#include <iostream>
#include <fstream>
#include <cassert>

#include <sdsl/suffix_arrays.hpp>

#include "divsufsort.h"

using namespace std;
using namespace sdsl;

int main(int argc, char** argv){
	if(argc != 2){
		cout << "ERROR! USE " << argv[0] << " <FILENAME>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	/* READING THE FILE */
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg();
	input_file.seekg(0, ios::beg);
	
	char * data = new char[n];
	
	input_file.read((char*)data, n);
	input_file.close();
	data[n] = 0;
	
	/* COMPUTING SA FROM DIVSUFSORT */
	int * sa_divsufsort = new int[n];
	int result = divsufsort((const unsigned char*)data, sa_divsufsort, n);
	if(result != 0){
		cout << "Error en cinstrucción suffix array divsufsort. Code " << result << endl;
		return -1;
	}
	
	/* COMPUTING SA FROM SDSL */
	csa_wt<wt_int<>> sa_sdsl;
	construct(sa_sdsl, filename, sizeof(char));
	
	/* COMPARING SUFFIX ARRAY */
	size_t differents = 0;
	cout << "index\tdivsufsort\tsdsl" << endl;
	for(size_t i = 0; i < n; i++){
		cout << i << "\t" << sa_divsufsort[i] << "\t" << sa_sdsl[i+1] << endl;
		if(sa_divsufsort[i] != sa_sdsl[i+1]){
			differents++;
		}
	}
	
	cout << "Encontramos " << differents << " diferencias de " << n << " elementos" << endl;
	
	return 0;
}