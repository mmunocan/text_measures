# generate related variables
import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.stats import kendalltau
from scipy.stats import kstest
from scipy.stats import norm
from scipy.stats import shapiro
from scipy.stats import anderson 
import seaborn as sns
import pandas as pd
from sklearn import preprocessing

if len(sys.argv) != 2:
	print("ERROR! USE " + sys.argv[0] + " <input_filename> ")
	sys.exit()

input_filename = sys.argv[1]

df = pd.read_csv(input_filename)
print(df)

pearson = df.corr()
pvalues = df.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(pearson.shape[0])

#pearson.to_csv('pearson.csv')
#pvalues.to_csv('pvalues.csv')

sns.set(font_scale=1.2)

fig, ax = plt.subplots()
ax.set_title('Pearson correlation coefficient')
dataplot = sns.heatmap(pearson, cmap="gist_gray", annot=True, fmt=".2f", vmin=0.5, cbar=False, annot_kws={"size":14},xticklabels=True, yticklabels=True) 
plt.tight_layout()
plt.show()


fig, ax = plt.subplots()
ax.set_title('Pearson correlation pvalues')
dataplot = sns.heatmap(pvalues, cmap="gist_gray", annot=True, fmt=".2f", cbar=True, annot_kws={"size":14},xticklabels=True, yticklabels=True) 
plt.tight_layout()
plt.show()
