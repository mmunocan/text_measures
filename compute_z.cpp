#include <iostream>
#include <chrono>
#include <vector>
#include <fstream>
#include <cassert>
#include <string>

using namespace std;

size_t compute_z_phrases(const string & input){
	size_t n = input.size();
	
	size_t n_phrases = 0;
	
	size_t i = 0;
	while(i < n){
		size_t j = i+1;
		size_t k = 1;
		while(j <= n){
			string p(input, i, k);
			if(k == 1){ // Is one number
				string prefix(input, 0, i);
				if(prefix.find(p) == string::npos){	// Symbol Not found
					n_phrases++;
					i = i+1;
					break;
				}
			}else{ // Is a substring
				string prefix(input, 0, i+k-1);
				if(prefix.find(p) == string::npos){	// Not found in S[0..i+k-1]
					n_phrases++;
					i = j-1;
					break;
				}
			}
			j++;
			k++;
		}
	}
	
	return n_phrases;
}

/*
 * input requires end 0 symbol
 */
size_t compute_z_measure(const string & filename){
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg();
	input_file.seekg(0, ios::beg);
	
	char * data = new char[n];
	
	input_file.read(data, n);
	input_file.close();
	
	string input(data, n);
	
	return compute_z_phrases(input);
	
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cout << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	auto start = chrono::high_resolution_clock::now();
	size_t z = compute_z_measure(filename);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << filename << ";" << z << ";" << time << endl;
	
	return 0;
}