#!/usr/bin/bash

if (($# != 3));
then
	echo "Error! include <origin_path> <output_path> <list_files>"
	exit
fi

origin_path=$1
output_path=$2
list_files=$3

mkdir -p $output_path
mkdir -p "$output_path/hk"
mkdir -p "$output_path/runs"
mkdir -p "$output_path/n"
mkdir -p "$output_path/z"
mkdir -p "$output_path/v"
mkdir -p "$output_path/g"
mkdir -p "$output_path/r"
mkdir -p "$output_path/delta"


i=0
while IFS= read -r line
do
	set -- $line
		filename=$1
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		# measures
		nohup ./compute_hk "$origin_path/$filename" > "$output_path/hk/$output_result" &
		nohup ./compute_runs "$origin_path/$filename" > "$output_path/runs/$output_result" &
		nohup ./compute_n "$origin_path/$filename" > "$output_path/n/$output_result" &
		
		nohup ./lzbg/lzBGL -f "$origin_path/$filename" > "$output_path/z/$output_result" &
		nohup ./compute_v "$origin_path/$filename" > "$output_path/v/$output_result" &
		nohup ./bal/repair "$origin_path/$filename" > "$output_path/g/$output_result" &
		nohup ./compute_r "$origin_path/$filename" > "$output_path/r/$output_result" &
		nohup ./compute_delta "$origin_path/$filename" > "$output_path/delta/$output_result" &
		
		
		i=$(($i + 1))
done < $list_files