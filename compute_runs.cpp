#include <iostream>
#include <chrono>
#include <fstream>
#include <cassert>

using namespace std;

size_t compute_runs(const string & filename){
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg();
	input_file.seekg(0, ios::beg);
	
	char * data = new char[n];
	
	input_file.read(data, n);
	input_file.close();
	
	string input(data, n);
	
	size_t runs = 1;
	for(size_t i = 1; i < n; i++){
		if(input[i-1] != input[i]){
			runs++;
		}
	}
	runs++;
	
	return runs;
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	auto start = chrono::high_resolution_clock::now();
	size_t runs = compute_runs(filename);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << filename << ";" << runs << ";" << time << endl;
	
	return 0;
}