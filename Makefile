CPP=g++

OBJECTS=
		
BINS=compute_hk compute_delta compute_runs compute_r append_zero compute_v compute_n cut_text show_dataset
		
CPPFLAGS=-fopenmp -O9 -g3 -std=c++17 -march=native -mtune=native -mavx2 -funroll-loops -finline-functions -fomit-frame-pointer -flto -Wall -lm -DNDEBUG -I ~/include -L ~/lib 
CPPFLAGSB=-lsdsl -ldivsufsort -ldivsufsort64 
DEST=.

%.o: %.c
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin: $(OBJECTS) $(BINS)

compute_hk:
	g++ $(CPPFLAGS) -o $(DEST)/compute_hk compute_hk.cpp $(OBJECTS) $(CPPFLAGSB)
	
compute_delta:
	g++ $(CPPFLAGS) -o $(DEST)/compute_delta compute_delta.cpp $(OBJECTS) $(CPPFLAGSB)
	
compute_runs:
	g++ $(CPPFLAGS) -o $(DEST)/compute_runs compute_runs.cpp $(OBJECTS) $(CPPFLAGSB)

compute_r:
	g++ $(CPPFLAGS) -o $(DEST)/compute_r compute_r.cpp $(OBJECTS) $(CPPFLAGSB)
	
append_zero:
	g++ $(CPPFLAGS) -o $(DEST)/append_zero append_zero.cpp $(OBJECTS) $(CPPFLAGSB)

compute_v:
	g++ $(CPPFLAGS) -o $(DEST)/compute_v compute_v.cpp $(OBJECTS) $(CPPFLAGSB)
	
compute_n:
	g++ $(CPPFLAGS) -o $(DEST)/compute_n compute_n.cpp $(OBJECTS) $(CPPFLAGSB)
	
cut_text:
	g++ $(CPPFLAGS) -o $(DEST)/cut_text cut_text.cpp $(OBJECTS) $(CPPFLAGSB)
	
show_dataset:
	g++ $(CPPFLAGS) -o $(DEST)/show_dataset show_dataset.cpp $(OBJECTS) $(CPPFLAGSB)

clean:
	rm -f $(OBJECTS) $(BINS)
	cd $(DEST); rm -f *.a $(BINS)
