#!/usr/bin/bash

make clean
cd bal
rm despair
rm idespair
rm repair
rm irepair
rm *.o
cd ..
cd lzbg
rm lzBGL
rm lzBGS
rm lzBGT
rm lziBGL
rm lziBGS
rm lziBGT
rm lziOG
rm lzOG
rm *.o
cd ..
