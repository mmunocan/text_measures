#include <iostream>
#include <chrono>
#include <fstream>
#include <cassert>


#include <omp.h>

#include <sdsl/lcp.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/wt_int.hpp>

using namespace std;
using namespace sdsl;


/*
 * input requires NOT end 0 symbol
 */
size_t compute_r_measure(const string & filename){
	csa_wt<> SA;
	construct(SA, filename, 1);
	size_t n = SA.size()-1;
	size_t runs = 1;
	
	for(size_t i = 1; i < n; i++){
		runs += SA.bwt[i-1] != SA.bwt[i];
	}
	
	return runs;
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	auto start = chrono::high_resolution_clock::now();
	size_t r = compute_r_measure(filename);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << filename << ";" << r << ";" << time << endl;
	
	return 0;
}