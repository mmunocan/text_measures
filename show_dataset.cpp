#include <iostream>
#include <fstream>
#include <cassert>

using namespace std;

void show_dataset(const string & filename){
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg();
	input_file.seekg(0, ios::beg);
	
	char * data = new char[n];
	input_file.read(data, n);
	input_file.close();
	string input(data, n);
	
	cout << "Texto original:" << endl;
	cout << input << endl;
	cout << "Texto numerificado: " << endl;
	for(size_t i = 0; i < n; i++){
		cout << "[" << (int)input[i] << "]";
	}
	cout << endl;
	
}

int main(int argc, char ** argv){
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	show_dataset(filename);
	
	return 0;
}