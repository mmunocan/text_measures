#include <iostream>
#include <fstream>
#include <cassert>

using namespace std;

void append_zero(const string & input_filename, const string & output_filename){
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg();
	input_file.seekg(0, ios::beg);
	
	char * data = new char[n+1];
	
	input_file.read(data, n);
	input_file.close();
	
	data[n] = (char)0;
	
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	
	output_file.write(data, (n+1));
	output_file.close();
	
}

int main(int argc, char ** argv){
	if(argc != 3){
		cerr << "ERROR! USE " << argv[0] << " <input_filename> <output_filename>" << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	string output_filename = argv[2];
	
	append_zero(input_filename, output_filename);
	
	return 0;
}