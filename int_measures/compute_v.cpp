#include <iostream>
#include <chrono>
#include <vector>
#include <fstream>
#include <cassert>
#include <algorithm>

#include <sdsl/lcp.hpp>
#include <sdsl/suffix_arrays.hpp>

using namespace std;
using namespace sdsl;

size_t contains_zeros(const string & filename){
	// read the input
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int * data = new int[n];
	
	input_file.read((char*)data, n * sizeof(int));
	input_file.close();
	
	size_t count_zeros = 0;
	for(size_t i = 0; i < n; i++){
		count_zeros += data[i] == 0;
	}
	
	return count_zeros;
}

size_t compute_v_phrases(const csa_wt<wt_int<>> & SA, const lcp_bitcompressed<> & LCP){
	size_t n = SA.size()-1;
	size_t n_phrases = 0;
	
	size_t i = 0;
	while(i < n){
		size_t SA_pos = SA.isa[i];
		
		if(SA_pos == 0){ // There are not a previous suffix to compare. The phrase is input[SA[SA_pos]].
			n_phrases++;
			
			i++;
		}else{ // Comparing the suffix with its previous in SA list
			
			size_t l = LCP[SA_pos];
			
			if(l == 0){ // There are not matching. The phrase is input[SA[SA_pos]].
				n_phrases++;
				
				i++;
			}else{ // There are a matching of size l. The phrase is input[SA[SA_pos]..SA[SA_pos]+l].
				n_phrases++;
				
				i += l;
			}
			
		}
	}
	return n_phrases;
	
}

/*
 * input requires NOT end 0 symbol
 */
size_t compute_v_measure(const string & filename){
	
	csa_wt<wt_int<>> SA;
	construct(SA, filename, sizeof(int));
	lcp_bitcompressed<> LCP;
	construct(LCP, filename, sizeof(int));
	
	return compute_v_phrases(SA, LCP);
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	size_t count_zeros = contains_zeros(filename);
	if(count_zeros > 0){
		cerr << filename << " contains " << count_zeros << " zeros" << endl;
		return -1;
	}
	
	auto start = chrono::high_resolution_clock::now();
	size_t v = compute_v_measure(filename);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << filename << ";" << v << ";" << time << endl;
	
	return 0;
}