#include <iostream>
#include <fstream>
#include <cassert>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

void from_char_to_int(const string & input_filename, const string & output_filename){
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg();
	input_file.seekg(0, ios::beg);
	
	unsigned char * data = new unsigned char[n];
	input_file.read((char*)data, n * sizeof(unsigned char));
	input_file.close();
	
	// Get all symbols
	map<unsigned char, unsigned int> symbols;
	for(size_t i = 0; i < n; i++){
		if(symbols.count(data[i]) == 0){
			symbols[data[i]] = 1;
		}
	}
	
	// sort symbols
	vector<unsigned char> sorted_symbols;
	for(auto it : symbols){
		sorted_symbols.push_back(it.first);
	}
	sort(sorted_symbols.begin(), sorted_symbols.end());
	
	// assign new symbols
	map<unsigned char, unsigned int> dictionary;
	unsigned int s = 1;
	for(unsigned char c : sorted_symbols){
		dictionary[c] = s;
		s++;
	}
	
	// generate new text
	unsigned int * int_data = new unsigned int[n];
	for(size_t i = 0; i < n; i++){
		int_data[i] = dictionary[data[i]];
	}
	
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)int_data, n*sizeof(unsigned int));
	output_file.close();
	
}

int main(int argc, char ** argv){
	
	if(argc != 3){
		cerr << "ERROR! USE " << argv[0] << " <input_filename> <output_filename>" << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	string output_filename = argv[2];
	
	from_char_to_int(input_filename, output_filename);
	
	cout << "Tranformation of '" << input_filename << "' to '" << output_filename << "' Ready! " << endl;
	
	return 0;
}