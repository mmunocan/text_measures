#include <iostream>
#include <chrono>
#include <utility>
#include <algorithm>

#include <omp.h>

#include <sdsl/lcp.hpp>
#include <sdsl/suffix_arrays.hpp>

using namespace sdsl;
using namespace std;

size_t contains_zeros(const string & filename){
	// read the input
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int * data = new int[n];
	
	input_file.read((char*)data, n * sizeof(int));
	input_file.close();
	
	size_t count_zeros = 0;
	for(size_t i = 0; i < n; i++){
		count_zeros += data[i] == 0;
	}
	
	return count_zeros;
}

void compute_LCP(const string & filename, int n, int * LCP){
	lcp_bitcompressed<> LCP_bit;
	construct(LCP_bit, filename, sizeof(int));
	
	#pragma omp parallel for
	for(int i = 0; i < n; i++){
		LCP[i] = LCP_bit[i];
	}
	
	sort(LCP, LCP + n);
	
}

void reduction_lcp(const int * LCP, int n, int * red_LCP){
	
	// Inicialization
	int start = LCP[0];
	#pragma omp parallel for
	for(int i = 0; i < start; i++){
		red_LCP[i] = 0;
	}
	red_LCP[start] = 1;
	
	int val;
	for(int k = 1; k < n; k++){
		val = LCP[k];
		int ant = LCP[k-1];
		if(ant == val){
			red_LCP[val]++;
		}else{
			red_LCP[val] = red_LCP[ant] + 1;
		}
	}
	
	int last = red_LCP[val];
	#pragma omp parallel for
	for(int i = val; i < n; i++){
		red_LCP[i] = last;
	}
	
}

void compute_D(const int * red_LCP, int * D, int n){
	
	#pragma omp parallel for
	for(int k = 0; k < n; k++){
		D[k] = red_LCP[k] - k;
	}
	
}

pair<size_t, size_t> compute_delta(const int * D, int n){
	size_t delta = 0;
	size_t final_k = 0;
	for(int k = 0; k < n; k++){
		size_t temp_delta = D[k] / (k+1);
		if(temp_delta > delta){
			delta = temp_delta;
			final_k = k+1;
		}
	}
	return make_pair(delta, final_k);
}


/*
 * input requires NOT end 0 symbol
 */
pair<size_t, size_t> compute_delta_measure(const string & filename){
	csa_wt<wt_int<>> SA;
	construct(SA, filename, sizeof(int));
	
	size_t n = SA.size()-1;
	
	int * LCP = new int[n];
	int * red_LCP = new int[n];
	int * D = new int[n];
	
	compute_LCP(filename, n, LCP);
	reduction_lcp(LCP, n, red_LCP);
	compute_D(red_LCP, D, n);
	return compute_delta(D, n);
	
	
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	size_t count_zeros = contains_zeros(filename);
	if(count_zeros > 0){
		cerr << filename << " contains " << count_zeros << " zeros" << endl;
		return -1;
	}
	
	auto start = chrono::high_resolution_clock::now();
	pair<size_t,size_t> delta = compute_delta_measure(filename);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << filename << ";" << delta.first << ";" << delta.second << ";" << time << endl;
	
	return 0;
}