#include <iostream>
#include <chrono>
#include <fstream>
#include <cassert>

#include <omp.h>

#include <sdsl/lcp.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/wt_int.hpp>

using namespace std;
using namespace sdsl;

size_t contains_zeros(const string & filename){
	// read the input
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int * data = new int[n];
	
	input_file.read((char*)data, n * sizeof(int));
	input_file.close();
	
	size_t count_zeros = 0;
	for(size_t i = 0; i < n; i++){
		count_zeros += data[i] == 0;
	}
	
	return count_zeros;
}

/*
 * input requires NOT end 0 symbol
 */
size_t compute_r_measure(const string & filename){
	csa_wt<wt_int<>> SA;
	construct(SA, filename, sizeof(int));
	size_t n = SA.size()-1;
	size_t runs = 1;
	
	for(size_t i = 1; i < n; i++){
		runs += SA.bwt[i-1] != SA.bwt[i];
	}
	
	return runs;
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	size_t count_zeros = contains_zeros(filename);
	if(count_zeros > 0){
		cerr << filename << " contains " << count_zeros << " zeros" << endl;
		return -1;
	}
	
	auto start = chrono::high_resolution_clock::now();
	size_t r = compute_r_measure(filename);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << filename << ";" << r << ";" << time << endl;
	
	return 0;
}